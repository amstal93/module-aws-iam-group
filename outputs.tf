#####
# Group
#####

output "iam_group_id" {
  value = element(concat(aws_iam_group.this.*.id, [null]), 0)
}

output "iam_group_arn" {
  value = element(concat(aws_iam_group.this.*.arn, [null]), 0)
}

output "iam_group_name" {
  value = element(concat(aws_iam_group.this.*.name, [null]), 0)
}

output "iam_group_unique_id" {
  value = element(concat(aws_iam_group.this.*.unique_id, [null]), 0)
}

#####
# Policy
#####

output "iam_policy_id" {
  value = element(concat(aws_iam_policy.this.*.id, [null]), 0)
}

output "iam_policy_arn" {
  value = element(concat(aws_iam_policy.this.*.arn, [null]), 0)
}

output "iam_policy_policy_id" {
  value = element(concat(aws_iam_policy.this.*.policy_id, [null]), 0)
}

output "iam_policy_path" {
  value = element(concat(aws_iam_policy.this.*.path, [null]), 0)
}

#####
# Role
#####

output "iam_role_id" {
  value = element(concat(aws_iam_role.this.*.id, [null]), 0)
}

output "iam_role_arn" {
  value = element(concat(aws_iam_role.this.*.arn, [null]), 0)
}

output "iam_role_name" {
  value = element(concat(aws_iam_role.this.*.name, [null]), 0)
}

output "iam_role_unique_id" {
  value = element(concat(aws_iam_role.this.*.unique_id, [null]), 0)
}
