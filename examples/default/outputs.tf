#####
# Default
#####

output "iam_group_id" {
  value = module.default.iam_group_id
}

output "iam_group_arn" {
  value = module.default.iam_group_arn
}

output "iam_group_name" {
  value = module.default.iam_group_name
}

output "iam_group_unique_id" {
  value = module.default.iam_group_unique_id
}

output "iam_policy_id" {
  value = module.default.iam_policy_id
}

output "iam_policy_arn" {
  value = module.default.iam_policy_arn
}

output "iam_policy_policy_id" {
  value = module.default.iam_policy_policy_id
}

output "iam_policy_path" {
  value = module.default.iam_policy_path
}

output "iam_role_id" {
  value = module.default.iam_role_id
}

output "iam_role_arn" {
  value = module.default.iam_role_arn
}

output "iam_role_name" {
  value = module.default.iam_role_name
}

output "iam_role_unique_id" {
  value = module.default.iam_role_unique_id
}

#####
# Advanced
#####

output "advanced_iam_group_id" {
  value = module.advanced.iam_group_id
}

output "advanced_iam_group_arn" {
  value = module.advanced.iam_group_arn
}

output "advanced_iam_group_name" {
  value = module.advanced.iam_group_name
}

output "advanced_iam_group_unique_id" {
  value = module.advanced.iam_group_unique_id
}

output "advanced_iam_policy_id" {
  value = module.advanced.iam_policy_id
}

output "advanced_iam_policy_arn" {
  value = module.advanced.iam_policy_arn
}

output "advanced_iam_policy_policy_id" {
  value = module.advanced.iam_policy_policy_id
}

output "advanced_iam_policy_path" {
  value = module.advanced.iam_policy_path
}

output "advanced_iam_role_id" {
  value = module.advanced.iam_role_id
}

output "advanced_iam_role_arn" {
  value = module.advanced.iam_role_arn
}

output "advanced_iam_role_name" {
  value = module.advanced.iam_role_name
}

output "advanced_iam_role_unique_id" {
  value = module.advanced.iam_role_unique_id
}
