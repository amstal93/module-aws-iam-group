#####
# General
#####

data "aws_organizations_organization" "this" {
  count = var.restrict_to_current_organization == true ? 1 : 0
}

data "aws_partition" "current" {}

#####
# Policy
#####

data "aws_iam_policy_document" "this" {
  count = var.iam_policy_enable ? 1 : 0

  source_json = var.iam_policy_additional_document

  statement {
    effect = "Allow"
    actions = [
      "sts:AssumeRole",
    ]
    resources = flatten([
      coalescelist(
        formatlist("arn:%s:iam::%s:role/%s", data.aws_partition.current.partition, [for account_id in var.account_ids : account_id], var.iam_role_name),
        formatlist("arn:%s:iam:::role/%s", data.aws_partition.current.partition, var.iam_role_name)
      ),
      aws_iam_role.this.*.arn,
      var.iam_policy_additional_allowed_switch_roles,
    ])

    dynamic "condition" {
      for_each = var.restrict_to_current_organization == true ? [1] : []

      content {
        test     = "StringEquals"
        values   = [data.aws_organizations_organization.this.*.id[0]]
        variable = "aws:PrincipalOrgID"
      }
    }
  }
}

#####
# Role
#####

data "aws_iam_policy_document" "this_assume_role" {
  count = var.iam_role_enable ? 1 : 0

  statement {
    effect = "Allow"
    actions = [
      "sts:AssumeRole",
    ]
    principals {
      identifiers = coalescelist(formatlist("arn:%s:iam::%s:root", data.aws_partition.current.partition, [for account_id in var.account_ids : account_id]), ["*"])
      type        = "AWS"
    }
  }
}
