# 0.0.1

- fix: makes sure `var.tags` is merged with local tags.

# 0.0.0

- Initial version
