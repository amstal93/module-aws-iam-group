#####
# General
#####

variable "account_ids" {
  description = <<-EOT
    List of “trusted” account IDs.
    The role created by this module will be assumable from all accounts listed.
    The policy created by this module will allow to assume the role in all listed accounts.
    If this list is empty, no restriction will be applied ; meaning the IAM group members will be able to assume `var.iam_role_name` in ANY account (except if `var.restrict_to_current_organization` is `true`)."
EOT
  type        = list(string)
  default     = []

  validation {
    condition     = alltrue([for account_id in var.account_ids : can(regex("^[0-9]{12}$", account_id))])
    error_message = "One or more of the “var.account_ids” does not match '^[0-9]{12}$'."
  }
}

variable "restrict_to_current_organization" {
  description = "Whether or not restricting members of the IAM group to assume role only within the current organization. This restriction is additional with `var.account_ids`: it does not replace it."
  type        = bool
  default     = false
}

variable "prefix" {
  type        = string
  description = "Prefix to be used for all resources names. Specifically useful for tests."
  default     = ""
}

variable "tags" {
  description = "Tags to be used in every resources created by this module."
  default     = {}
}

#####
# Group
#####

variable "iam_group_enable" {
  type        = bool
  description = "Whether ot not to create the IAM group. Generally, the group needs to be created only once, in the main account."
  default     = true
}

variable "iam_group_name" {
  type        = string
  description = "Name of the group to create. Ignored if `var.iam_group_enable` is `false`."
  default     = "example"

  validation {
    condition     = can(regex("^[_+=,\\.@a-zA-Z0-9-]{1,128}$", var.iam_group_name))
    error_message = "The var.iam_group_name must match “^[_+=,\\.@a-zA-Z0-9-]{1,128}$”."
  }
}

variable "iam_group_policy_arns" {
  type        = map(string)
  description = "ARNs of the policies to attach to the IAM Group. Keys are ignored. Ignored if `var.iam_group_enable` is `false`."
  default     = {}

  validation {
    condition     = var.iam_group_policy_arns == null || alltrue([for i in var.iam_group_policy_arns : can(regex("^arn:aws(-us-gov|-cn)?:iam:([a-z]{2}-[a-z]{4,10}-[1-9]{1})?:([0-9]{12}|aws):policy/[a-zA-Z0-9_+=,\\./@-]{1,128}$", i))])
    error_message = "One or more of the “var.iam_group_policy_arns” does not match '^arn:aws(-us-gov|-cn)?:iam:([a-z]{2}-[a-z]{4,10}-[1-9]{1})?:([0-9]{12}|aws):policy/[a-zA-Z0-9_+=,\\./@-]{1,128}$'."
  }
}

#####
# Policy
#####

variable "iam_policy_enable" {
  type        = bool
  description = "Whether ot not to create the IAM policy to link to the group. By default, this policy will allow the `var.iam_group_name` to assume the `var.iam_role_name` in all the `var.account_ids`. Generally, the policy needs to be created only once alongside the group in the main account. This policy document can be extended by using `var.iam_policy_additional_document`."
  default     = true
}

variable "iam_policy_name" {
  type        = string
  description = "Name of the policy to create for the group. Ignored if `var.iam_policy_enable` is `false`."
  default     = "example-policy"

  validation {
    condition     = can(regex("^[_+=,\\.@a-zA-Z0-9-]{1,128}$", var.iam_policy_name))
    error_message = "The var.iam_policy_name must match “^[_+=,\\.@a-zA-Z0-9-]{1,128}$”."
  }
}

variable "iam_policy_additional_allowed_switch_roles" {
  type        = list(string)
  description = "List of role ARNs to be added in the IAM policy linked to the group. The IAM group members will be allowed to assume these roles. The roles can be in any accounts."
  default     = []

  validation {
    condition     = length(var.iam_policy_additional_allowed_switch_roles) == 0 || alltrue([for i in var.iam_policy_additional_allowed_switch_roles : can(regex("^arn:aws(-us-gov|-cn)?:iam:([a-z]{2}-[a-z]{4,10}-[1-9]{1})?:([0-9]{12}|aws)?:role/[a-zA-Z0-9+=,\\./@-]+$", i))])
    error_message = "One or more of the “var.iam_policy_additional_allowed_switch_roles” does not match '^arn:aws(-us-gov|-cn)?:iam:([a-z]{2}-[a-z]{4,10}-[1-9]{1})?:([0-9]{12}|aws)?:role/[a-zA-Z0-9+=,\\./@-]+$'."
  }
}

variable "iam_policy_additional_document" {
  type        = string
  description = "JSON document that will be merged with the default document of the IAM policy. By default, the policy only allow to switch role. This will be ignored if `var.iam_policy_enable` is `false."
  default     = null
}

#####
# Role
#####

variable "iam_role_enable" {
  type        = bool
  description = "Whether ot not to create the IAM role that will be assumable by the `var.iam_group_name`. Generally, the roles should be created in all the accounts."
  default     = true
}

variable "iam_role_name" {
  type        = string
  description = "Name of the role to create for the group. Ignored if `var.iam_role_enable` is `false`."
  default     = "example-role"

  validation {
    condition     = var.iam_role_name == null || can(regex("^[_+=,\\.@a-zA-Z0-9-]{1,128}$", var.iam_role_name))
    error_message = "The var.iam_role_name must match “^[_+=,\\.@a-zA-Z0-9-]{1,128}$”."
  }
}

variable "iam_role_policy_arns" {
  type        = map(string)
  description = "ARNs of the policies to attach to the IAM role. Keys are ignored. Ignored if `var.iam_role_enable` is `false`."
  default     = {}

  validation {
    condition     = var.iam_role_policy_arns == null || alltrue([for i in var.iam_role_policy_arns : can(regex("^arn:aws(-us-gov|-cn)?:iam:([a-z]{2}-[a-z]{4,10}-[1-9]{1})?:([0-9]{12}|aws):policy/[a-zA-Z0-9_+=,\\./@-]{1,128}$", i))])
    error_message = "One or more of the “var.iam_role_policy_arns” does not match '^arn:aws(-us-gov|-cn)?:iam:([a-z]{2}-[a-z]{4,10}-[1-9]{1})?:([0-9]{12}|aws):policy/[a-zA-Z0-9_+=,\\./@-]{1,128}$'."
  }
}
