locals {
  tags = merge(
    {
      managed-by = "terraform"
      origin     = "gitlab.com/wild-beavers/terraform/module-aws-iam-group"
    },
    var.tags
  )
}

#####
# Group
#####

locals {
  iam_group_name = format("%s%s", var.prefix, var.iam_group_name)
}

resource "aws_iam_group" "this" {
  count = var.iam_group_enable ? 1 : 0

  name = local.iam_group_name
}

#####
# Policy
#####

locals {
  iam_policy_name = format("%s%s", var.prefix, var.iam_policy_name)
}

resource "aws_iam_policy" "this" {
  count = var.iam_policy_enable ? 1 : 0

  policy = data.aws_iam_policy_document.this.*.json[0]
  name   = local.iam_policy_name

  tags = local.tags
}

resource "aws_iam_group_policy_attachment" "this_base" {
  count = var.iam_policy_enable && var.iam_group_enable ? 1 : 0

  group      = aws_iam_group.this.*.name[0]
  policy_arn = aws_iam_policy.this.*.arn[0]
}

resource "aws_iam_group_policy_attachment" "this" {
  for_each = var.iam_group_enable ? var.iam_group_policy_arns : {}

  group      = aws_iam_group.this.*.name[0]
  policy_arn = each.value
}

#####
# Role
#####

locals {
  iam_role_name = format("%s%s", var.prefix, var.iam_role_name)
}

resource "aws_iam_role" "this" {
  count = var.iam_role_enable ? 1 : 0

  name        = local.iam_role_name
  description = "${local.iam_group_name} group role. All members of the group can assume this role."

  tags               = local.tags
  assume_role_policy = data.aws_iam_policy_document.this_assume_role.*.json[0]
}

resource "aws_iam_role_policy_attachment" "this" {
  for_each = var.iam_role_enable ? var.iam_role_policy_arns : {}

  role       = aws_iam_role.this.*.name[0]
  policy_arn = each.value
}
